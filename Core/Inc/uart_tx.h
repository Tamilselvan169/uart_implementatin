/*
 * uart_tx.h
 *
 *  Created on: May 2, 2024
 *      Author: tamilselvan
 */
#ifndef UART_TX_H_
#define UART_TX_H_

#include "main.h"
#include <stdint.h>
#include <string.h>
// Define SystemFB structure
typedef struct {
    uint32_t variable1;
    uint32_t variable2;
    uint32_t variable3;
    uint32_t variable4;
    uint32_t variable5;
    uint32_t variable6;
    uint32_t variable7;
    uint32_t variable8;
    uint32_t variable9;
    uint32_t variable10;
    uint32_t variable11;
    uint32_t variable12;
    uint32_t variable13;
    uint32_t variable14;
    uint32_t variable15;
    uint32_t variable16;
    uint32_t variable17;
    uint32_t variable18;
} SystemFB;

// Define ACKMessage structure
typedef struct {
    uint32_t variable1;
    uint32_t variable2;
    uint32_t variable3;
    uint32_t variable4;
    uint32_t variable5;
    uint32_t variable6;
    uint32_t variable7;
    uint32_t variable8;
    uint32_t variable9;
    uint32_t variable10;
    uint32_t variable11;
    uint32_t variable12;
    uint32_t variable13;
    uint32_t variable14;
    uint32_t variable15;
    uint32_t variable16;
} ACKMessage;

// Declare variables as extern
extern SystemFB data1;
extern ACKMessage data2;

//typedef struct {
//    uint32_t data[18];
//} SystemFB;
//
//// Define ACKMessage structure
//typedef struct {
//    uint32_t data[16];
//} ACKMessage;
//
//// Declare variables as extern
//extern SystemFB systemfb_array;
//extern ACKMessage ackmsg_array;
// Function prototypes

void SerializeData_1(char *buffer);
void TransmitData_1(void);
void SerializeData_2(char *buffer);
void TransmitData_2(void);

#endif /* INC_UART_TX_H_ */
