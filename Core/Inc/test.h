/*
 * test.h
 *
 *  Created on: May 7, 2024
 *      Author: tamilselvan
 */

#ifndef INC_TEST_H_
#define INC_TEST_H_
#include "main.h"
#include "stdbool.h"
#include "string.h"
#include "stdio.h"
#include "stdint.h"
// Define the structure for the command data
extern uint8_t rxData;
extern UART_HandleTypeDef huart3;  // Declare huart3 as an external variable

typedef struct {
    int ros_data_ref;
    int throttle_value;
    int direction_ctrl_data;
    float steering_kp;
    float steering_ki;
    float steering_kd;
    int steering_directing;
    float steering_preceision;
    float steering_motor_set_point;
    int steering_en;
    int auto_manual_val;
    int mast_op_data;
    int brake_val;
    int color_value;
    int horn_value;
    int diffuser_value;
    int indicator_value;
    int hokuyo_val;
    int brake_angle;
    int brake_pwr;
    int lidar_ctrl[1];
    int veh_mode;
} command;

// Function prototypes
void transmit_ack(command cmd);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void executeCommand(command cmd);

// Callback function prototypes
void cb_handshake(uint8_t handshake_value);
void cb_mode(uint16_t mode_value);
void cb_throttle(int8_t throttle_value);
void cb_steering(double steering_value);
void cb_mast_angle(int8_t mast_angle_value);
void cb_utility(uint32_t utility_value);
void cb_safety_control(uint32_t safety_ctrl_value);
void cb_brake(int8_t brake_value);

#endif /* INC_TEST_H_ */
