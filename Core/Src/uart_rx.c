/*
 * uart.c
 *
 *  Created on: May 2, 2024
 *      Author: tamilselvan
 */
//
//#include "uart_rx.h"
//
//#include "string.h"
//#include "stdio.h"
//
//extern UART_HandleTypeDef huart3;  // Declare huart3 as an external variable
//
//void handle_Throttle(int throttle);
//void handle_Steering(int steering);
//void handle_Mast(int mast);
//void handle_Rgb_light(int rgb_light);
//void handle_Indicator_light(int indicator_light);
//void handle_Diffuser_light(int diffuser_light);
//void handle_Horn(int horn);
//
//
//
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
//	static uint8_t rxBuffer[50];  // Buffer for command
//	    static uint8_t rxIndex = 0;   // Index for buffer
//
//	    // Check if the callback is triggered by USART3
//	    if (huart->Instance == USART3) {
//	        // Check if received data is newline or carriage return
//	        if (rxData == '\n' || rxData == '\r') {
//	            // Null-terminate the received command string
//	            rxBuffer[rxIndex] = '\0';
//
//	            // Parse received command string to extract throttle, steering, and mast values
//	            command cmd;
//	            sscanf((char*)rxBuffer, "(%d,%d,%d,%d,%d,%d,%d)", &cmd.throttle, &cmd.steering, &cmd.mast,&cmd.rgb_light,&cmd.indicator_light,&cmd.diffuser_light,&cmd.horn);
//
//	            // Execute the command
//	            executeCommand(cmd);
//
//	            // Clear the buffer and reset index for next command
//	            rxIndex = 0;
//	            memset(rxBuffer, 0, sizeof(rxBuffer));
//	        } else {
//	            // Accumulate characters until newline or carriage return is received
//	            rxBuffer[rxIndex++] = rxData;
//	        }
//
//	        // Continue to receive data using interrupt
//	        HAL_UART_Receive_IT(&huart3, &rxData, 1);
//	    }
//}
//
//
//void transmit_ack(int data) {
//	 char response[50]; // Buffer for response message
//	    snprintf(response, sizeof(response), "Received throttle is %d\r\n", data);
//	    // Transmit response via UART
//	     HAL_UART_Transmit(&huart3, (uint8_t*)response, strlen(response), HAL_MAX_DELAY);
//}
//
//void executeCommand(command cmd) {
//    // Perform actions based on received command values
//    handle_Throttle(cmd.throttle);
//    handle_Steering(cmd.steering);
//    handle_Mast(cmd.mast);
//    handle_Rgb_light(cmd.rgb_light);
//    handle_Indicator_light(cmd.indicator_light);
//    handle_Diffuser_light(cmd.diffuser_light);
//    handle_Horn(cmd.horn);
//}
//
//void handle_Throttle(int throttle) {
//	if (throttle == 0) {
//	        // Turn off LD1 (throttle 0)
//	        HAL_GPIO_WritePin(GPIOB, LD1_Pin, GPIO_PIN_RESET);
//	        HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
//	        HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_RESET);
//	    }
//	    else if (throttle == 10) {
//	        // Turn on LD1 (throttle 100)
//	        HAL_GPIO_WritePin(GPIOB, LD1_Pin, GPIO_PIN_SET);
//	        HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
//	        HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_RESET);
//	    }else if (throttle == 50) {
//	        // Turn on LD1 (throttle 100)
//	        HAL_GPIO_WritePin(GPIOB, LD1_Pin, GPIO_PIN_RESET);
//	        HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
//	        HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_RESET);
//	    }
//	    else if (throttle == 100) {
//	           // Turn on LD1 (throttle 100)
//	        HAL_GPIO_WritePin(GPIOB, LD1_Pin, GPIO_PIN_RESET);
//	        HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
//	        HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_SET);
//	       }
//	    transmit_ack(throttle);
//
//}
//
//void handle_Steering(int steering) {
//    // Implementation
//}
//
//void handle_Mast(int mast) {
//    // Implementation
//}
//
//void handle_Rgb_light(int rgb_light) {
//    // Implementation
//}
//
//void handle_Indicator_light(int indicator_light) {
//    // Implementation
//}
//
//void handle_Diffuser_light(int diffuser_light) {
//    // Implementation
//}
//
//void handle_Horn(int horn) {
//    // Implementation
//}




