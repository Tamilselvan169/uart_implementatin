/*
 * test.c
 *
 *  Created on: May 7, 2024
 *      Author: tamilselvan
 */
#include "test.h"

extern UART_HandleTypeDef huart3;
uint8_t rx_Data; // Declare rx_Data variable

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
    static uint8_t rxBuffer[50];
    static uint8_t rxIndex = 0;

    if (huart->Instance == USART3) {
        if (rx_Data == '\n' || rx_Data == '\r') {
            rxBuffer[rxIndex] = '\0';
            command cmd;
            sscanf((char *)rxBuffer, "( %d, %d, %d, %f, %f, %f, %d, %f, %f, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)",
                   &cmd.ros_data_ref, &cmd.throttle_value, &cmd.direction_ctrl_data,
                   &cmd.steering_kp, &cmd.steering_ki, &cmd.steering_kd, &cmd.steering_directing,
                   &cmd.steering_preceision, &cmd.steering_motor_set_point, &cmd.steering_en,
                   &cmd.auto_manual_val, &cmd.mast_op_data, &cmd.brake_val, &cmd.color_value,
                   &cmd.horn_value, &cmd.diffuser_value, &cmd.indicator_value, &cmd.hokuyo_val,
                   &cmd.brake_angle, &cmd.brake_pwr, &cmd.lidar_ctrl[0], &cmd.veh_mode);
//            transmit_ack(cmd);   //To check what data recving on UART
            executeCommand(cmd);
            rxIndex = 0;
            memset(rxBuffer, 0, sizeof(rxBuffer));
        } else {
            rxBuffer[rxIndex++] = rx_Data;
        }
        HAL_UART_Receive_IT(huart, &rx_Data, 1);
    }
}

void transmit_ack(command cmd) {
    char response[2000];
    snprintf(response, sizeof(response), "Received data: %d, %d, %d, %.2f, %.2f, %.2f, %d, %.2f, %.2f, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\r\n",
            cmd.ros_data_ref, cmd.throttle_value, cmd.direction_ctrl_data,
            cmd.steering_kp, cmd.steering_ki, cmd.steering_kd, cmd.steering_directing,
            cmd.steering_preceision, cmd.steering_motor_set_point, cmd.steering_en,
            cmd.auto_manual_val, cmd.mast_op_data, cmd.brake_val, cmd.color_value,
            cmd.horn_value, cmd.diffuser_value, cmd.indicator_value, cmd.hokuyo_val,
            cmd.brake_angle, cmd.brake_pwr, cmd.lidar_ctrl[0], cmd.veh_mode);
    HAL_UART_Transmit(&huart3, (uint8_t *)response, strlen(response), HAL_MAX_DELAY);
}

void executeCommand(command cmd) {
    // Implement your command execution logic here
	 cb_throttle(cmd.throttle_value);

}

void cb_handshake(uint8_t handshake_value) {
    // Implement your callback logic here
}

void cb_mode(uint16_t mode_value) {
    // Implement your callback logic here
}

void cb_throttle(int8_t throttle_value) {
	char response[50]; // Declare a character buffer to hold the formatted string
		if (throttle_value == 0) {
		        // Turn off LD1 (throttle 0)
		        HAL_GPIO_WritePin(GPIOB, LD1_Pin, GPIO_PIN_RESET);
		        HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
		        HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_RESET);
		    }
		    else if (throttle_value == 10) {
		        // Turn on LD1 (throttle 100)
		        HAL_GPIO_WritePin(GPIOB, LD1_Pin, GPIO_PIN_SET);
		        HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
		        HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_RESET);
		    }else if (throttle_value == 50) {
		        // Turn on LD1 (throttle 100)
		        HAL_GPIO_WritePin(GPIOB, LD1_Pin, GPIO_PIN_RESET);
		        HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
		        HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_RESET);
		    }
		    else if (throttle_value == 100) {
		           // Turn on LD1 (throttle 100)
		        HAL_GPIO_WritePin(GPIOB, LD1_Pin, GPIO_PIN_RESET);
		        HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
		        HAL_GPIO_WritePin(GPIOB, LD3_Pin, GPIO_PIN_SET);
		       }
	    // Format the throttle value and store it in the response buffer
	    snprintf(response, sizeof(response), "Received throttle is %d\r\n", throttle_value);

	    // Transmit response via UART
	    HAL_UART_Transmit(&huart3, (uint8_t*)response, strlen(response), HAL_MAX_DELAY);
    // Implement your callback logic here
}

void cb_steering(double steering_value) {
    // Implement your callback logic here
}

void cb_mast_angle(int8_t mast_angle_value) {
    // Implement your callback logic here
}

void cb_utility(uint32_t utility_value) {
    // Implement your callback logic here
}

void cb_safety_control(uint32_t safety_ctrl_value) {
    // Implement your callback logic here
}

void cb_brake(int8_t brake_value) {
    // Implement your callback logic here
}
