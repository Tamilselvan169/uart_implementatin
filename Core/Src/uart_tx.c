/*
 * uart_tx.c
 *
 *  Created on: May 2, 2024
 *      Author: tamilselvan
 */
#include <uart_tx.h>
#include <stdio.h>
#include <string.h>

extern UART_HandleTypeDef huart3;

// SystemFB data initialization
SystemFB data1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};

// ACKMessage data initialization
ACKMessage data2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

/// Implement the SerializeData_1 function
void SerializeData_1(char *buffer) {
    sprintf(buffer, "{\"SystemFB1\": %ld, \"SystemFB2\": %ld, \"SystemFB3\": %ld,\"SystemFB4\": %ld, \"SystemFB5\": %ld,"
    		" \"SystemFB6\": %ld, \"SystemFB7\": %ld, \"SystemFB8\": %ld, \"SystemFB9\": %ld, \"SystemFB10\": %ld,"
            " \"SystemFB11\": %ld, \"SystemFB12\": %ld,\"SystemFB13\": %ld, \"SystemFB14\": %ld, \"SystemFB15\": %ld,"
            " \"SystemFB16\": %ld, \"SystemFB17\": %ld, \"SystemFB18\": %ld} \r\n",
            data1.variable1, data1.variable2, data1.variable3, data1.variable4, data1.variable5, data1.variable6,
			data1.variable7, data1.variable8, data1.variable9, data1.variable10, data1.variable11, data1.variable12,
			data1.variable13, data1.variable14, data1.variable15, data1.variable16, data1.variable17, data1.variable18);
//    sprintf(buffer, "{\"SystemFB\": [%ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld]} \r\n",
//               systemfb_array.data[0], systemfb_array.data[1], systemfb_array.data[2], systemfb_array.data[3],
//               systemfb_array.data[4], systemfb_array.data[5], systemfb_array.data[6], systemfb_array.data[7],
//               systemfb_array.data[8], systemfb_array.data[9], systemfb_array.data[10], systemfb_array.data[11],
//               systemfb_array.data[12], systemfb_array.data[13], systemfb_array.data[14], systemfb_array.data[15],
//               systemfb_array.data[16], systemfb_array.data[17]);
}

// Implement the TransmitData_1 function
void TransmitData_1(void) {
    // Create a buffer to hold the serialized data
    char tx_buffer_1[500]; // Adjust size as needed

    // Serialize data into JSON format
    SerializeData_1(tx_buffer_1);

    // Transmit the JSON data over UART
    if (HAL_UART_Transmit(&huart3, (uint8_t *)tx_buffer_1, strlen(tx_buffer_1), HAL_MAX_DELAY) != HAL_OK) {
        // Error handling if transmission fails
        // You can add specific actions or logging here
    }
}

// Implement the SerializeData_2 function
void SerializeData_2(char *buffer) {
    sprintf(buffer, "{ \"ACKMsg1\": %ld, \"ACKMsg2\": %ld, \"ACKMsg3\": %ld,\"ACKMsg4\": %ld, \"ACKMsg5\": %ld,"
    		" \"ACKMsg6\": %ld, \"ACKMsg7\": %ld, \"ACKMsg8\": %ld, \"ACKMsg9\": %ld, \"ACKMsg10\": %ld,"
    		" \"ACKMsg11\": %ld, \"ACKMsg12\": %ld, \"ACKMsg13\": %ld, \"ACKMsg14\": %ld, \"ACKMsg15\": %ld, \"ACKMsg16\": %ld} \r\n",
            data2.variable1, data2.variable2, data2.variable3, data2.variable4, data2.variable5, data2.variable6,
			data2.variable7, data2.variable8, data2.variable9, data2.variable10, data2.variable11, data2.variable12,
			data2.variable13, data2.variable14, data2.variable15, data2.variable16);
//
//    sprintf(buffer, "{\"ACKMsg\": [%ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld, %ld]} \r\n",
//               ackmsg_array.data[0], ackmsg_array.data[1], ackmsg_array.data[2], ackmsg_array.data[3],
//               ackmsg_array.data[4], ackmsg_array.data[5], ackmsg_array.data[6], ackmsg_array.data[7],
//               ackmsg_array.data[8], ackmsg_array.data[9], ackmsg_array.data[10], ackmsg_array.data[11],
//               ackmsg_array.data[12], ackmsg_array.data[13], ackmsg_array.data[14], ackmsg_array.data[15]);
}


// Implement the TransmitData_2 function
void TransmitData_2(void) {
    // Create a buffer to hold the serialized data
    char tx_buffer_2[500]; // Adjust size as needed

    // Serialize data into JSON format
    SerializeData_2(tx_buffer_2);

    // Transmit the JSON data over UART
    if (HAL_UART_Transmit(&huart3, (uint8_t *)tx_buffer_2, strlen(tx_buffer_2), HAL_MAX_DELAY) != HAL_OK) {
        // Error handling if transmission fails
        // You can add specific actions or logging here
    }
}
